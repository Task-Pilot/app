import { IconButton, Paper, Stack, Switch, Toolbar, Typography } from '@mui/material'
import CheckIcon from '@mui/icons-material/Check';
import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { Box } from '@mui/system';
import { requireUpdate, setView, updateCategoryState } from '../store/slices/taskState';
import AddIcon from "@mui/icons-material/Add"
import TrashIcon from "@mui/icons-material/Delete"
import { deleteCategory, updateCategory } from '../services/client-api';

const Category = ({category_id, name, active}) => {
  console.log(active)
  
  const dispatch = useDispatch()
  const [isActive, setActive] = useState(active)

  const onDelete = async () => {
    const result = await deleteCategory(category_id)
    if (result.success) {
      dispatch(requireUpdate())
    }
  }

  const toggleActive = async () => {
    setActive(!isActive)
    dispatch(updateCategoryState({
      categoryId: category_id,
      active: !isActive
    }))

    await updateCategory({category_id, name, active: !isActive})
  }

  return <Box
    sx={{
      p: 2,
      display: "flex",
      alignItems: "center",
      justifyContent: "space-between",
    }}
  >
    <Stack
      direction="row"
      alignItems="center"
      spacing={1}
    >
      <Switch checked={isActive} onClick={toggleActive} />
      <Typography
        variant="h6"
      >
        {name}
      </Typography>
    </Stack>
    <IconButton
      onClick={onDelete}
    >
      <TrashIcon />
    </IconButton>
  </Box>
}

const EditCategories = ({onClose}) => {
  const dispatch = useDispatch()
  const categories = useSelector(state => state.tasks.categories)

  return (
    <Paper
      sx={{
        backgroundColor: theme => theme.palette.background.default,
      }}
    >
      <Toolbar
        sx={{
          backgroundColor: theme => theme.palette.background.paper,
          justifyContent: "space-between",
          alignItems: "center",
        }}
      >
        <Typography variant="h6" color="inherit">
          Edit Categories
        </Typography>
        <IconButton
          color="primary"
          onClick={onClose}
        >
          <CheckIcon color="inherit" />
        </IconButton>
      </Toolbar>
      <Stack>
        {categories.length > 0 && categories.map(category => 
          <Category 
            key={category.category_id} 
            category_id={category.category_id} 
            name={category.name}
            active={category.active}
          />)
        }
        {categories.length === 0 && 
        <Typography 
          color="inherit"
          sx={{p:2}}
        >
          No categories
        </Typography>}
        <Box
          sx={{
            display: "flex",
            alignItems: "center",
            cursor: "pointer",
            p: 2,
          }}
          onClick={() => dispatch(setView("addCategory"))}
        >
          <AddIcon color="primary" fontSize="small" />
          <Typography color="primary">
            Add Category
          </Typography>
        </Box>
      </Stack>
    </Paper>
  )
}

export default EditCategories