import { Container, Typography, CircularProgress, Box } from '@mui/material'
import React from 'react'

const LoadingScreen = ({ text }) => {
  return (
    <Container
      sx={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        flexDirection: "column",
        height: "100%",
      }}
    >
      <Box
        sx={{
          p: 5
        }}
      >
        <CircularProgress />
      </Box>
      <Typography
        variant="body1"
      >
        { text }
      </Typography>
    </Container>
  )
}

export default LoadingScreen