import React from 'react'
import { Toolbar, Container, Stack, Typography, IconButton } from "@mui/material"
import MenuIcon from "@mui/icons-material/Menu"
import CategoryIcon from "@mui/icons-material/Category"
import AddCircleIcon from "@mui/icons-material/AddCircle"
import { useDispatch } from 'react-redux'
import { setMenuDrawerOpen, setView } from '../store/slices/taskState'

const TaskViewToolbar = () => {
  const dispatch = useDispatch()

  return (
    <Toolbar
      sx={{
        backgroundColor: theme => theme.palette.background.paper,
        mb: 2
      }}

    >
      <Container
        maxWidth="md"
        sx={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'space-between',
          cursor: "pointer",
        }}
      >
        <Stack
          direction="row"
          spacing={1}
          alignItems="center"
          onClick={() => {
            dispatch(setMenuDrawerOpen(true))
          }}
        >
          <MenuIcon />
          <Typography
            variant="h6"
          >
            Task Pilot
          </Typography>
        </Stack>

        <Stack
          direction="row"
          spacing={1}
        >
          <IconButton
            color="inherit"
            onClick={() => dispatch(setView("editCategories"))}
          >
            <CategoryIcon />
          </IconButton>
          <IconButton
            color="inherit"
            onClick={() => dispatch(setView("createTask"))}
          >
            <AddCircleIcon />
          </IconButton>
        </Stack>
      </Container>
    </Toolbar>
  )
}

export default TaskViewToolbar