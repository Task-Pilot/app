import { Button, Box, CircularProgress, FormControl, InputLabel, MenuItem, Paper, Select, Stack, TextField, Toolbar, Typography, IconButton } from '@mui/material'
import { DesktopDatePicker } from '@mui/x-date-pickers'
import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { addTask } from '../services/client-api'
import { requireUpdate } from '../store/slices/taskState'
import CloseIcon from "@mui/icons-material/Close"
import { resetCreateTaskView, setTaskCategory, setTaskDate, setTaskStatus, setTaskText } from '../store/slices/createTaskViewState'
import moment from 'moment'
import { parseDate } from '../util/date-util'

const CreateTask = ({ onClose }) => {
  const dispatch = useDispatch()
  const categories = useSelector(state => state.tasks.categories)
  const [requestState, setRequestState] = useState(null)
  const {
    date,
    status,
    text,
    categoryId
  } = useSelector(state => state.createTaskView)

  const onCreate = async (e) => {
    e.preventDefault()

    setRequestState("pending")
    const task = {
      date,
      status,
      text,
      category_id: categoryId,
    }

    const result = await addTask(task)

    if (result.success) {
      onClose()
      dispatch(resetCreateTaskView())
      dispatch(requireUpdate())
    } else {
      console.log("Error creating task:", result.message)
      setRequestState("error")
    }
  }

  return (
    <Paper
      sx={{
        backgroundColor: theme => theme.palette.background.default
      }}
    >
      <Toolbar
        sx={{
          backgroundColor: theme => theme.palette.background.paper,
          justifyContent: "space-between"
        }}
      >
        <Typography variant="h6" color="inherit">
          Create Task
        </Typography>
        <IconButton
          onClick={onClose}
        >
          <CloseIcon />
        </IconButton>
      </Toolbar>
      <Box
        sx={{ p: 2 }}
      >
        <Stack
          spacing={2}
        >

          <TextField
            id="text"
            label="Task"
            name="text"
            variant="filled"
            placeholder="Describe your task"
            value={text}
            onChange={e => dispatch(setTaskText(e.target.value))}
          />

          <DesktopDatePicker
            label="Due date"
            inputFormat="MM/DD/YYYY"
            onChange={date => dispatch(setTaskDate(parseDate(date.toDate())))}
            value={moment(date)}
            renderInput={(params) => <TextField variant="standard" {...params} />}
          />

          <Stack
            spacing={1}
            direction="row"
          >
            <FormControl
              variant="standard"
              fullWidth
            >
              <InputLabel id="status">Status</InputLabel>
              <Select
                name="status"
                labelId="status"
                label="Status"
                value={status}
                onChange={e => dispatch(setTaskStatus(e.target.value))}
              >
                <MenuItem value={0}>Not started</MenuItem>
                <MenuItem value={1}>In progress</MenuItem>
                <MenuItem value={2}>Completed</MenuItem>
              </Select>
            </FormControl>
            <FormControl
              variant="standard"
              fullWidth
            >
              <InputLabel id="category">Category</InputLabel>
              <Select
                name="category"
                labelId="category"
                label="Category"
                value={categoryId}
                onChange={e => dispatch(setTaskCategory(e.target.value))}
              >
                <MenuItem value={categoryId}>None</MenuItem>
                {categories.filter(category => category.active).map(category =>
                  <MenuItem
                    key={category.category_id}
                    value={category.category_id}
                  >
                    {category.name}
                  </MenuItem>)}
              </Select>
            </FormControl>
          </Stack>

          <Stack
            spacing={1}
            direction="row"
          >
            <Button
              disabled={requestState === "pending"}
              variant="contained"
              color="primary"
              endIcon={requestState === "pending" && <CircularProgress color="inherit" size="20px" />}
              onClick={onCreate}
            >
              Create
            </Button>
            <Button
              variant="text"
              color="secondary"
              onClick={onClose}
            >
              Cancel
            </Button>
          </Stack>
          {requestState === "error" && <Typography color="error">Error creating task.</Typography>}
        </Stack>
      </Box>
    </Paper>
  )
}

export default CreateTask