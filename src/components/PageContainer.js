import { Box } from '@mui/material'
import React from 'react'

const PageContainer = ({ children }) => {
  return (
    <Box
      sx={{
        height: "100%",
        display: "flex",
        flexDirection: "column",
        justifyContent: "space-between",
        gap: "50px",
      }}
    >
      { children } 
    </Box>
  )
}

export default PageContainer