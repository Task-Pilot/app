import { Box, Chip, IconButton, Paper, Stack, Typography, Menu, MenuItem, TextField } from '@mui/material'
import React, { useRef, useState } from 'react'
import RadioButtonUncheckedIcon from '@mui/icons-material/RadioButtonUnchecked';
import RadioButtonCheckedIcon from '@mui/icons-material/RadioButtonChecked';
import CalendarMonthIcon from '@mui/icons-material/CalendarMonth';
import DeleteIcon from '@mui/icons-material/Delete';
import { updateTask } from '../services/client-api';
import { useDispatch, useSelector } from 'react-redux';
import { requireUpdate, setView } from '../store/slices/taskState';
import { CalendarPicker } from '@mui/x-date-pickers';
import moment from 'moment';
import { deparseDate, parseDate } from '../util/date-util';
import { setDeleteConfirmationTaskId } from '../store/slices/deleteTaskConfirmationState';

const Task = ({task}) => {
  const {task_id, text, date, status, category_id} = task
  const dispatch = useDispatch()
  const statusRef = useRef()
  const textFieldRef = useRef()
  const categoryRef = useRef()
  const dateRef = useRef()
  const [editing, setEditing] = useState(null)
  const [newText, setNewText] = useState(text)
  const categories = useSelector(state => state.tasks.categories)
  // Default date string is compact.
  const dateObject = deparseDate(date)
  
  const onStatusUpdate = async (status) => {
    setEditing(null)
    
    const result = await updateTask({...task, status})
    if (result.success) {
      dispatch(requireUpdate())
    }
  }

  const onCategoryUpdate = async (category_id) => {
    setEditing(null)

    const result = await updateTask({...task, category_id})
    if (result.success) {
      dispatch(requireUpdate())
    }
  }

  const onDateUpdate = async (date) => {
    setEditing(null)


    const result = await updateTask( { ...task, date: parseDate(date) } )
    if (result.success) {
      dispatch(requireUpdate())
    }
  }

  const updateText = async () => {
    const result = await updateTask({...task, text: newText})
    if (result.success) {
      dispatch(requireUpdate())
    }
  }

  const onDeleteClick = () => {
    dispatch(setDeleteConfirmationTaskId(task_id))
    dispatch(setView("deleteTask"))
  }

  var categoryName = "None"
  if (category_id !== null) {
    const category = categories.find(c => c.category_id === category_id)
    if (category !== undefined) {
      categoryName = category.name
    }
  }

  return (<>
    <Paper
      sx={{
        p: 1,
        display: 'flex',
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
      }}
    >
      <Box
        sx={{
          display: 'flex',
          flexDirection: "row",
          alignItems: "center",
        }}
      >
        <IconButton
          size="small"
          ref={statusRef}
          onClick={() => setEditing("status")}
        >
          {status === 0 && <RadioButtonUncheckedIcon color="primary"/>}
          {status !== 0 && <RadioButtonCheckedIcon color={status === 1 ? "warning" : "success"} />}
        </IconButton>
        <IconButton
          size="small"
          sx={{mr: 1}}
          ref={dateRef}
          onClick={() => setEditing("date")}
        >
          <CalendarMonthIcon />
        </IconButton>
        <Stack
          spacing={1}
          direction="row"
          justifyContent="flex-start"
          alignItems="center"
        >
          {editing !== "text" && <Typography
            sx={{
              cursor: "pointer",
              fontWeight: "600",
              letterSpacing: "1px",
              pr: 1,
            }}
            onClick={() => {
              setEditing("text")
            }}
          >
            {text}
          </Typography>}
          {<form
            onSubmit={(e) => {e.preventDefault(); setEditing("text"); updateText()}}
          >
            <TextField
              sx={{display: editing !== "text" && "none"}}
              inputRef={textFieldRef}
              size="small" 
              value={newText} 
              onChange={(e) => setNewText(e.target.value)}
            />
          </form>}
          <Chip 
            clickable
            ref={categoryRef}
            label={categoryName} 
            color="secondary"
            onClick={() => setEditing("category")}
          />
        </Stack>
      </Box>
      <IconButton
        size="small"
        sx={{ml: 1}}
        onClick={onDeleteClick}
      >
        <DeleteIcon />
      </IconButton>
    </Paper>
    <Menu
      anchorEl={statusRef.current}
      open={editing === "status"}
      onClose={() => setEditing(null)}
    >
      <MenuItem 
        value={0} 
        onClick={() => onStatusUpdate(0)}
        selected={status === 0}
      >
        <RadioButtonUncheckedIcon 
          color="primary" 
          fontSize="inherit"
          sx={{mr: 1}}
        />
        Not started
      </MenuItem>
      <MenuItem 
        value={1} 
        onClick={() => onStatusUpdate(1)}
        selected={status === 1}
      >
        <RadioButtonCheckedIcon 
          color="warning" 
          fontSize="inherit"
          sx={{mr: 1}}
        />
        In progress
      </MenuItem>
      <MenuItem 
        value={2} 
        onClick={() => onStatusUpdate(2)}
        selected={status === 2}
      >
        <RadioButtonCheckedIcon 
          color="success" 
          fontSize="inherit"
          sx={{mr: 1}}
        />
        Completed
      </MenuItem>
    </Menu>
    <Menu
      anchorEl={categoryRef.current}
      open={editing === "category"}
      onClose={() => setEditing(null)}
    >
      <MenuItem value={"None"} onClick={() => onCategoryUpdate("None")}>None</MenuItem>
      {categories.filter(category => category.active).map(category => 
        <MenuItem 
          key={category.category_id}
          value={category.category_id}
          onClick={() => onCategoryUpdate(category.category_id)}
        >
          {category.name}
        </MenuItem>
      )}
    </Menu>
    <Menu
      anchorEl={dateRef.current}
      open={editing === "date"}
      onClose={() => setEditing(null)}
    >
      <CalendarPicker
        date={moment(dateObject)}
        onChange={(date) => onDateUpdate(date.toDate())}
      />
    </Menu>
    </>
  )
}

export default Task