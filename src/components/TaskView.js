import React from 'react'
import { Stack, FormControl, InputLabel, Select, MenuItem, Paper, Typography } from '@mui/material'
import { useDispatch, useSelector } from 'react-redux'
import groupTasks from '../util/group-tasks'
import { setCategoryFilter, setStatusFilter } from '../store/slices/taskState'
import TaskGroup from './TaskGroup'
import LoadingScreen from './LoadingScreen'

const TaskView = () => {
  const tasks = useSelector(state => state.tasks.tasks)
  const categories = useSelector(state => state.tasks.categories)
  const dispatch = useDispatch()

  const { categoryFilter, statusFilter, refreshing } = useSelector(state => state.tasks)

  const groups = groupTasks(tasks)

  return (
    <Stack
      spacing={1}
    >
      <Stack
        direction="row"
        spacing={1}
      >
        <FormControl
          sx={{ minWidth: "200px" }}
          variant="standard"
          size="small"
        >
          <InputLabel id="category_filter">Category</InputLabel>
          <Select
            labelId="category_filter"
            value={categoryFilter}
            onChange={(e) => dispatch(setCategoryFilter(e.target.value))}
          >
            <MenuItem value={"any"}>Any</MenuItem>
            {categories.filter(category => category.active).map(category =>
              <MenuItem key={category.category_id} value={category.category_id}>{category.name}</MenuItem>
            )}
          </Select>
        </FormControl>
        <FormControl
          sx={{ minWidth: "200px" }}
          variant="standard"
          size="small"
        >
          <InputLabel id="status_filter">Status</InputLabel>
          <Select
            labelId="status_filter"
            value={statusFilter}
            onChange={(e) => dispatch(setStatusFilter(e.target.value))}
          >
            <MenuItem value={"any"}>Any</MenuItem>
            <MenuItem value={"hide-completed"}>Hide Completed</MenuItem>
            <MenuItem value={"only-completed"}>Only Completed</MenuItem>
          </Select>
        </FormControl>
      </Stack>
      {
        refreshing !== true ? <>
          <Stack
            spacing={3}
            sx={{
              pb: 2,
            }}
          >
            {tasks !== null && groups.map(group => <TaskGroup key={group.text} group={group} />)}
          </Stack>
          {tasks !== null && tasks.length === 0 && <Typography sx={{ p: 2 }}>Press the + button at the top right to create a task.</Typography>}
          {tasks === null && <Paper sx={{ p: 2 }}><Typography sx={{ p: 2 }}>Failed to load tasks!</Typography></Paper>}
        </> : <LoadingScreen text="Refreshing tasks..." />
      }
    </Stack>
  )
}

export default TaskView