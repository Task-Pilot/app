import React from 'react'
import { Modal, Box } from '@mui/material'

const CenterModal = ({maxWidth = "600px", children, open, onClose, ...props}) => {
  return (
    <Modal
      open={open}
      onClose={onClose}
      {...props}
    >
      <Box sx={{
        maxWidth: maxWidth,
        left: "50%",
        top: "50%",
        position: "relative",
        transform: "translate(-50%, -50%)",
        p: 2
      }}>
        {children}
      </Box>
    </Modal>
  )
}

export default CenterModal