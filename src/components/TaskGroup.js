import { Stack, Typography } from '@mui/material'
import React from 'react'
import Task from './Task'
import ArrowUp from "@mui/icons-material/KeyboardArrowUp"
import ArrowDown from "@mui/icons-material/KeyboardArrowDown"
import { useDispatch, useSelector } from 'react-redux'
import { setGroupOpen } from '../store/slices/taskState'


const TaskGroup = ({group}) => {
  const dispatch = useDispatch()
  var open = useSelector(state => state.tasks.groupsOpen)[group.text]
  if (open === undefined) {
    open = true
  }
  const setOpen = (value) => {
    dispatch(setGroupOpen({groupKey: group.text, open: value}))
  }
  const {text, tasks} = group
  return (
    <Stack
      spacing={1}
      
    >
      <Stack 
        spacing={1}
        direction="row"
        alignItems="center"
        sx={{
          cursor: "pointer"
        }}
  
        onClick={() => setOpen(!open)}
      >
        {open ? <ArrowUp fontSize="12px"/> : <ArrowDown fontSize="12px"/>}
        <Typography variant="h6">{text}</Typography>
        {!open && <Typography variant="h6" color="gray">({tasks.length})</Typography>}
      </Stack>
      {open && <Stack spacing={1}>
        {tasks.map(task => <Task key={task.task_id} task={task} />)}
      </Stack>}
    </Stack>
  )
}

export default TaskGroup