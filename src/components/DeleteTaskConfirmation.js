import { Box, Button, CircularProgress, IconButton, Paper, Stack, Toolbar, Typography } from '@mui/material'
import CloseIcon from "@mui/icons-material/Close"
import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { requireUpdate, setView } from '../store/slices/taskState'
import { deleteTask } from '../services/client-api'

const DeleteTaskConfirmation = () => {
  const taskId = useSelector(state => state.deleteTaskConfirmation.taskId)
  const [requestState, setRequestState] = useState(null)
  const dispatch = useDispatch()

  const onDelete = async () => {
    console.log(taskId)
    setRequestState("pending")
    const result = await deleteTask(taskId)
    if (result.success) {
      setRequestState("success")
      dispatch(requireUpdate())
      onClose()
    } else {
      setRequestState("fail")
    }
  }

  const onClose = () => {
    dispatch(setView(null))
  }

  return (
    <Paper
      sx={{
        backgroundColor: theme => theme.palette.background.default
      }}
    >
      <Toolbar
        sx={{
          backgroundColor: theme => theme.palette.background.paper,
          justifyContent: "space-between"
        }}
      >
        <Typography variant="h6" color="inherit">
          Delete Task?
        </Typography>
        <IconButton
          onClick={onClose}
        >
          <CloseIcon />
        </IconButton>
      </Toolbar>
      <Box
        sx={{
          p: 2
        }}
      >
        <Typography>
          Are you sure you want to permanently delete this task?
        </Typography>
        <Stack
          sx={{
            mt: 2
          }}
          spacing={1}
          direction="row"
        >
          <Button
            variant="contained"
            color="primary"
            onClick={onDelete}
            endIcon={
              requestState === "pending" && <CircularProgress color="inherit" size="16px" />
            }
          >
            Delete
          </Button>
          <Button
            variant="text"
            color="secondary"
            onClick={onClose}
          >
            Cancel
          </Button>
        </Stack>
      </Box>
        
    </Paper>
  )
}

export default DeleteTaskConfirmation