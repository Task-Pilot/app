import { Divider, Drawer, List, ListItem, ListItemButton, ListItemText } from '@mui/material'
import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useNavigate } from 'react-router-dom'
import { logout } from '../services/client-api'
import { setSession } from '../store/slices/authState'
import { setMenuDrawerOpen } from '../store/slices/taskState'

const MenuDrawer = () => {
  const menuDrawerOpen = useSelector(state => state.tasks.menuDrawerOpen)
  const session = useSelector(state => state.auth.session)
  const dispatch = useDispatch()
  const nav = useNavigate()

  const closeMenu = () => dispatch(setMenuDrawerOpen(false))

  const onLogout = async () => {
    dispatch(setSession(null))
    closeMenu()
    logout()
    nav("/login")
  }

  return (
    <Drawer
      anchor="left"
      open={menuDrawerOpen}
      onClose={closeMenu}
    >
      <List
        sx={{
          minWidth: "200px",
        }}
      >
        <ListItem>
          <ListItemText>
            Task Pilot
          </ListItemText>
        </ListItem>
        <ListItemButton>
          <ListItemText>
            {session.name}
          </ListItemText>
        </ListItemButton>
        <Divider />
        <ListItemButton
          onClick={onLogout}
        >
          <ListItemText>
            Logout
          </ListItemText>
        </ListItemButton>
      </List>
    </Drawer>
  )
}

export default MenuDrawer