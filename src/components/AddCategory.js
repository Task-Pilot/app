import { Paper, Toolbar, Typography, IconButton, Stack, Box, TextField, Button, CircularProgress } from "@mui/material"
import CloseIcon from "@mui/icons-material/Close"
import { useState } from "react"
import { addCategory } from "../services/client-api"
import { useDispatch } from "react-redux"
import { requireUpdate } from "../store/slices/taskState"

const CreateCategory = ({onClose}) => {
  const [requestState, setRequestState] = useState(false)
  const [categoryName, setCategoryName] = useState("")
  const dispatch = useDispatch()

  const onCreate = async () => {
    setRequestState("pending")
    const result = await addCategory(categoryName)
    if (result.success) {
      setRequestState("success")
      dispatch(requireUpdate())
      onClose()
    } else {
      setRequestState("error")
    }
  }

  return (
    <Paper
      sx={{
        backgroundColor: theme => theme.palette.background.default
      }}
    >
      <Toolbar
        sx={{
          backgroundColor: theme => theme.palette.background.paper,
          justifyContent: "space-between"
        }}
      >
        <Typography variant="h6" color="inherit">
          Add Category
        </Typography>
        <IconButton
          onClick={onClose}
        >
          <CloseIcon />
        </IconButton>
      </Toolbar>
      <Box
        sx={{p: 2}}
      >
        <Stack
          spacing={2}
        >
          <TextField 
            variant="filled"
            label="Category Name"
            value={categoryName}
            onChange={(e) => setCategoryName(e.target.value)}
            fullWidth
          />
          <Stack
            spacing={1}
            direction="row"
          >
            <Button
              disabled={requestState === "pending"}
              variant="contained"
              color="primary"
              endIcon={requestState === "pending" && <CircularProgress color="inherit" size="20px" />}
              onClick={onCreate}
            >
              Create
            </Button>
            <Button
              variant="text"
              color="secondary"
              onClick={onClose}
            >
              Cancel
            </Button>
          </Stack>
          {requestState === "error" && <Typography color="error">Error creating category.</Typography>}
        </Stack>
      </Box>
    </Paper>
  )
}

export default CreateCategory