import { Button, Box, Paper, Stack, Typography, Container } from '@mui/material'
import React from 'react'
import GitLab from "../assets/gitlab-logo-200.svg"


const Footer = () => {
  return (
    <Paper
      sx={{
        minHeight: "80px",
      }}
      component="footer"
    >
      <Container 
        maxWidth="sm"
        sx={{
          height: "100%",
          p: 2,
          display: "flex",
          alignItems: "center"
        }}
      >
        <Stack
          direction="row"
          alignItems="center"
          justifyContent="space-between"
          spacing={4}
          sx={{
            width: "100%",
          }}
        >
          <Typography>
            By Braden Godley
          </Typography>
          <Button
              sx={{
                p: 0,
                width: "120px"
              }}
              variant="contained"
              href="https://gitlab.com/Task-Pilot"
            >
              <Box
                component="img"
                sx={{
                  height: "36px"
                }}
                src={GitLab} 
              />
            </Button>
        </Stack>
      </Container>
    </Paper>
  )
}

export default Footer