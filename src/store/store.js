import { configureStore } from "@reduxjs/toolkit"
import authReducer from "./slices/authState"
import tasksReducer from "./slices/taskState"
import createTaskViewReducer from "./slices/createTaskViewState"
import deleteTaskConfirmationReducer from "./slices/deleteTaskConfirmationState"

export default configureStore({
  reducer: {
    auth: authReducer,
    tasks: tasksReducer,
    createTaskView: createTaskViewReducer,
    deleteTaskConfirmation: deleteTaskConfirmationReducer
  },
})