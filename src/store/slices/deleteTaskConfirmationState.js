import { createSlice } from "@reduxjs/toolkit"

const initialState = {
  taskId: null
}

export const slice = createSlice({
  name: 'deleteTaskConfirmation',
  initialState,
  reducers: {
    setDeleteConfirmationTaskId: (state, action) => {
      state.taskId = action.payload
    }
  },
})

export const { 
  setDeleteConfirmationTaskId
} = slice.actions
export default slice.reducer