import { createSlice } from "@reduxjs/toolkit"

const slice = createSlice({
  name: 'auth',
  initialState: {
    session: null,
  },
  reducers: {
    setSession: (state, action) => {
      const session = action.payload
      state.session = session
    },
    clearSession: (state) => {
      state.session = null
    }
  },
})

export const { setSession, clearSession } = slice.actions
export default slice.reducer