import { createSlice } from "@reduxjs/toolkit"
import { parseDate } from "../../util/date-util"

const initialState = {
  text: "",
  categoryId: "None",
  date: parseDate(new Date()),
  status: 0
}

export const slice = createSlice({
  name: 'createTaskView',
  initialState,
  reducers: {
    setTaskText: (state, action) => {
      state.text = action.payload
    },
    setTaskCategory: (state, action) => {
      state.categoryId = action.payload
    },
    setTaskDate: (state, action) => {
      state.date = action.payload
    },
    setTaskStatus: (state, action) => {
      state.status = action.payload
    },
    resetCreateTaskView: (state) => {
      state.text = initialState.text
      state.categoryId = initialState.categoryId
      state.date = initialState.date
      state.status = initialState.status
    }
  },
})

export const { 
  setTaskCategory,
  setTaskDate,
  setTaskStatus,
  setTaskText,
  resetCreateTaskView
} = slice.actions
export default slice.reducer