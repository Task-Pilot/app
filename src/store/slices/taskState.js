import { createSlice } from "@reduxjs/toolkit"

export const slice = createSlice({
  name: 'tasks',
  initialState: {
    tasks: [],
    categories: [],

    requiresUpdate: true,
    view: null,
    groupsOpen: {},
    menuDrawerOpen: false,

    categoryFilter: "any",
    statusFilter: "hide-completed",
    refreshing: false
  },
  reducers: {
    setTasks: (state, action) => {
      const tasks = action.payload.tasks
      const categories = action.payload.categories
      state.requiresUpdate = false
      state.categories = categories
      state.tasks = tasks
    },
    updateCategoryState: (state, action) => {
      const { categoryId, active } = action.payload
      
      state.categories.forEach((category) => {
        if (category.category_id === categoryId) {
          category.active = active
          console.log("updated")
        }
      })
    },
    requireUpdate: (state) => {
      state.requiresUpdate = true
    },
    setView: (state, action) => {
      state.view = action.payload
    },
    setGroupOpen: (state, action) => {
      console.log(action.payload)
      const { groupKey, open } = action.payload
      state.groupsOpen[groupKey] = open
    },
    setMenuDrawerOpen: (state, action) => {
      state.menuDrawerOpen = action.payload
    },
    setCategoryFilter: (state, action) => {
      state.categoryFilter = action.payload
    },
    setStatusFilter: (state, action) => {
      state.statusFilter = action.payload
    },
    setRefreshing: (state, action) => {
      state.refreshing = action.payload
    }
  },
})

export const { 
  setTasks, 
  requireUpdate, 
  updateCategoryState,
  setView, 
  setGroupOpen, 
  setMenuDrawerOpen,
  setCategoryFilter,
  setStatusFilter,
  setRefreshing
} = slice.actions
export default slice.reducer