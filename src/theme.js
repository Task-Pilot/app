import { createTheme } from "@mui/material";

let theme = createTheme({
  palette: {
    mode: "dark",
    primary: {
      main: "hsl(240, 70%, 70%)",
      light: "hsl(240, 70%, 80%)",
      dark: "hsl(240, 70%, 65%)",
    },
    secondary: {
      main: "hsl(40, 100%, 60%)",
      light: "hsl(40, 100%, 66%)",
      dark: "hsl(40, 100%, 50%)",
      contrastText: "#000",
    },
    background: {
      paper: "hsl(240, 60%, 14%)",
      default: "hsl(240, 60%, 5%)"
    }
  },
  shape: {
    borderRadius: 0
  },
  typography: {
    fontFamily: "Inter, -apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Oxygen, Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue, sans-serif",
    h1: {
      fontSize: 48,
      fontWeight: 700,
    },
    h2: {
      fontSize: 36,
      fontWeight: 700,
    },
    h3: {
      fontSize: 28,
      fontWeight: 700,
    },
    h4: {
      fontSize: 22,
      fontWeight: 700,
    },
    h5: {
      fontSize: 20,
      fontWeight: 700,
    },
    h6: {
      fontSize: 18,
      fontWeight: 700,
    },
    body1: {
      fontSize: 16,
      fontWeight: 400,
    },
    subtitle1: {
      fontSize: 18,
    },
    subtitle2: {
      fontSize: 16,
      fontWeight: 400,
    },
    button: {
      letterSpacing: 3
    }
  },
  components: {
    MuiButtonBase: {
      defaultProps: {
        disableRipple: true,
      },
    },
  }
})

export default theme