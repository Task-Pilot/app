const defaultHeaders = {
  "Content-Type": "application/json",
  "Accept": "application/json"
}

async function getSession() {
  /* Checks if your authenticated.
    Returns session if authenticated
    Otherwise returns null
  */
  const res = await fetch("/auth/", {
    method: "POST",
    headers: defaultHeaders
  })

  const result = await res.json()
  if (result.authenticated) {
    return result.session
  } else {
    return null
  }
}

async function getCategories() {
  /* Requests a list of all categories */
  const res = await fetch("/api/getCategories", {
    method: "POST",
    headers: defaultHeaders,
  })

  let json = await res.json()

  json.categories = json.categories.map(category => {
    return {
      ...category,
      active: category.active === 1,
    }
  })

  return json
}

async function addCategory(name) {
  /* Adds a new category */
  const res = await fetch("/api/addCategory", {
    method: "POST",
    headers: defaultHeaders,
    body: JSON.stringify({ name })
  })

  return await res.json()
}

async function updateCategory(category) {
  const res = await fetch("/api/updateCategory", {
    method: "PUT",
    headers: defaultHeaders,
    body: JSON.stringify({
      category: category
    })
  })

  return await res.json()
}

async function deleteCategory(category_id) {
  /* Deletes a category */
  const res = await fetch("/api/deleteCategory", {
    method: "DELETE",
    headers: defaultHeaders,
    body: JSON.stringify({ category_id })
  })

  return await res.json()
}

async function getTasks({ limit = 10000, page = 0, sort_by = "soonest", categoryFilter = "any", statusFilter = "any" }) {
  const res = await fetch("/api/getTasks", {
    method: "POST",
    headers: defaultHeaders,
    body: JSON.stringify({ limit, page, sort_by, statusFilter, categoryFilter })
  })
  return await res.json()
}

async function addTask({ text, status = 0, date = null, category_id = null}) {
  const res = await fetch("/api/addTask", {
    method: "POST",
    headers: defaultHeaders,
    body: JSON.stringify({ task: { text, status, date, category_id } })
  })

  return await res.json()
}

async function updateTask(task) {

  const res = await fetch("/api/updateTask", {
    method: "PUT",
    headers: defaultHeaders,
    body: JSON.stringify({task: task})
  })

  return await res.json()
}

async function deleteTask(task_id) {
  const res = await fetch("/api/deleteTask", {
    method: "DELETE",
    headers: defaultHeaders,
    body: JSON.stringify({ task_id })
  })

  return await res.json()
}

async function login(email, password) { 
  const res = await fetch('/auth/login', {
    method: 'POST',
    headers: defaultHeaders,
    body: JSON.stringify({
      email: email,
      password: password,
    })
  })

  return await res.json()
}

async function register(name, email, password) {
  const res = await fetch('/auth/register', {
    method: 'POST',
    headers: defaultHeaders,
    body: JSON.stringify({
      email: email,
      password: password,
      name: name,
    })
  })

  return await res.json()
}

async function logout() {
  const res = await fetch('/auth/logout', {
    method: 'POST',
    headers: defaultHeaders,
  })

  return await res.json()
}

export { 
  getSession,
  getCategories,
  addCategory,
  updateCategory,
  deleteCategory,
  getTasks,
  addTask,
  updateTask,
  deleteTask,
  login,
  register,
  logout,
}