/* eslint-disable react-hooks/exhaustive-deps */
import { Container } from '@mui/material'
import { getSession, getTasks, getCategories } from '../../services/client-api'
import React, { useEffect } from 'react'
import CenterModal from '../../components/CenterModal'
import CreateTask from '../../components/CreateTask'
import { useDispatch, useSelector } from 'react-redux'
import { setRefreshing, setTasks, setView } from '../../store/slices/taskState'
import { setSession } from '../../store/slices/authState'
import { requireUpdate } from '../../store/slices/taskState'
import EditCategories from '../../components/EditCategories'
import AddCategory from '../../components/AddCategory'
import { useNavigate } from 'react-router-dom'
import MenuDrawer from '../../components/MenuDrawer';
import TaskViewToolbar from '../../components/TaskViewToolbar'
import TaskView from '../../components/TaskView'
import LoadingScreen from '../../components/LoadingScreen'
import DeleteTaskConfirmation from '../../components/DeleteTaskConfirmation'

const TaskPilotView = () => {
  const { view } = useSelector(state => state.tasks)
  const dispatch = useDispatch()

  return <>
    <TaskViewToolbar/>
    <Container
      maxWidth="md"
    >
      <TaskView />
    </Container>
    <CenterModal
      open={view === "createTask"}
      onClose={() => dispatch(setView(null))}
    >
      <CreateTask onClose={() => dispatch(setView(null))} />
    </CenterModal>

    <CenterModal
      open={view === "editCategories"}
      onClose={() => dispatch(setView(null))}
    >
      <EditCategories onClose={() => dispatch(setView(null))} />
    </CenterModal>

    <CenterModal
      open={view === "addCategory"}
      onClose={() => dispatch(setView("editCategories"))}
    >
      <AddCategory onClose={() => dispatch(setView("editCategories"))} />
    </CenterModal>
    <CenterModal
      open={view === "deleteTask"}
      onClose={() => dispatch(setView(null))}
    >
      <DeleteTaskConfirmation />
    </CenterModal>
    <MenuDrawer />
  </>
}

const TaskPilot = () => {
  const dispatch = useDispatch()
  const nav = useNavigate()

  const session = useSelector(state => state.auth.session)
  
  const { 
    categoryFilter, 
    statusFilter, 
    requiresUpdate, 
  } = useSelector(state => state.tasks)

  const refreshData = async () => {
    console.log("refresh")
    dispatch(setRefreshing(true))
    try {
      const cresult = await getCategories()
      const tresult = await getTasks({ categoryFilter, statusFilter })
      if (cresult.success && tresult.success) {
        dispatch(setTasks({
          categories: cresult.categories,
          tasks: tresult.tasks
        }))
      }
    } catch (e) {
      console.error(e)
    }
    dispatch(setRefreshing(false))
  }

  // Authenticate session
  useEffect(() => {
    async function auth() {
      const result = await getSession()
      if (result !== null) {
        dispatch(setSession(result))
      } else {
        dispatch(setSession(null))
        nav("/login")
      }
    }

    auth()
  }, [])

  // Load tasks when an update is required
  useEffect(() => {
    async function refresh() {
      if (requiresUpdate && session !== null) {
        await refreshData()
      }
    }
    refresh()
  }, [requiresUpdate, session])

  useEffect(() => {
    dispatch(requireUpdate())
  }, [categoryFilter, statusFilter])

  useEffect(() => {
    window.addEventListener("focus", () => {
      dispatch(requireUpdate())
    })
  }, [])

  return (
    <>
      {session !== null ? <TaskPilotView /> : <LoadingScreen text="Signing in..." />}
    </>
  )
}

export default TaskPilot