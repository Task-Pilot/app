import { Container, Typography } from '@mui/material'
import React from 'react'

const NotFoundPage = () => {
  return (
    <Container maxWidth="md">
      <Typography variant="h1">404 not found</Typography>
    </Container>
  )
}

export default NotFoundPage