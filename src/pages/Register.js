import React, { useState } from 'react'
import { Box, Button, CircularProgress, Container, Paper, TextField, Typography } from "@mui/material"
import { Stack } from '@mui/system'
import { register } from '../services/client-api'
import CenterModal from '../components/CenterModal'
import { useNavigate } from "react-router-dom"
import PageContainer from '../components/PageContainer'
import Footer from '../components/Footer'
import { KeyboardArrowLeft } from '@mui/icons-material'

const Register = () => {
  const nav = useNavigate()
  const [requestState, setRequestState] = useState(null)
  const [requestResponse, setRequestResponse] = useState("")

  const submit = async (e) => {
    e.preventDefault()

    setRequestState('pending')

    const formData = new FormData(e.target)

    const name = formData.get('name')
    const email = formData.get('email')
    const password = formData.get('password')
    const password_confirm = formData.get('password_confirm')

    if (password !== password_confirm) {
      setRequestState('failure')
      setRequestResponse('Passwords do not match')
      return
    }

    const result = await register(name, email, password)

    setRequestResponse(result.message)
    if (result.success) {
      setRequestState('success')
      nav("/")
    } else {
      setRequestState('failure')
    }
  }

  return (<PageContainer>
    <Container maxWidth="sm">
      <Box
        sx={{
          my: 10,
          textAlign: 'center',
        }}
      >
        <Typography
            variant="h2"
          >
          Task Pilot
        </Typography>
        <Typography>
          Stay on course, stay ahead
        </Typography>
      </Box>

      <Button
        sx={{
          mb: 1
        }}
        startIcon={
          <KeyboardArrowLeft/>
        }
        onClick={() => nav("/login")}
      >
        Back to Sign In
      </Button>
      <Paper
        sx={{
          p: 2
        }}
      >
        
        <Stack
          component="form"
          spacing={2}
          onSubmit={submit}
        >
          <Typography variant="h4">Create an account</Typography>
          <TextField 
            name="name"
            label="Name"
            type="text"
            variant="filled"
            required
            fullWidth 
          />
          <TextField 
            name="email"
            label="Email"
            type="email"
            variant="filled"
            required
            fullWidth 
          />
          <TextField 
            name="password"
            label="Password"
            type="password"
            variant="filled"
            required
            fullWidth 
          />
          <TextField 
            name="password_confirm"
            label="Confirm password"
            type="password"
            variant="filled"
            required
            fullWidth 
          />
          <Button
            type="submit"
            variant="contained"
            color="primary"
            endIcon={requestState === "pending" && <CircularProgress color="inherit" size="20px" />}
          >
            Register
          </Button>
        </Stack>
      </Paper>
    </Container>
    <Footer/>

    <CenterModal
      open={requestState === 'failure'}
      onClose={() => setRequestState(null)}
    >
      <Paper sx={{
        p: 2
      }}>
        <Typography variant="h6">Registration failed</Typography>
        <Typography variant="body">{requestResponse}</Typography>
      </Paper>
    </CenterModal>

    <CenterModal
      open={requestState === 'success'}
      onClose={() => setRequestState(null)}
    >
      <Paper sx={{
        p: 2
      }}>
        <Typography variant="h6">Registration succeeded</Typography>
        <Typography variant="body">{requestResponse}</Typography>
      </Paper>
    </CenterModal>
  </PageContainer>)
}

export default Register