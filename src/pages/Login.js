import React, { useState } from 'react'
import { Box, Button, CircularProgress, Container, Paper, TextField, Typography } from "@mui/material"
import { Stack } from '@mui/system'
import { useNavigate } from 'react-router-dom'
import { login } from '../services/client-api'
import CenterModal from '../components/CenterModal'
import Footer from '../components/Footer'
import PageContainer from '../components/PageContainer'

const Login = () => {
  const nav = useNavigate()
  const [requestState, setRequestState] = useState(null)

  const submit = async (e) => {
    e.preventDefault()

    setRequestState('pending')

    const formData = new FormData(e.target)

    const email = formData.get('email')
    const password = formData.get('password')

    const result = await login(email, password)
    
    if (result.success) {
      nav("/")
    } else {
      setRequestState('failure')
    }
  }

  return (<PageContainer>
    <Container
      maxWidth="sm"
    >
      <Box
        sx={{
          py: 10,
          textAlign: 'center',
        }}
      >
        <Typography
            variant="h2"
          >
          Task Pilot
        </Typography>
        <Typography>
          Stay on course, stay ahead
        </Typography>
      </Box>

      <Paper
        sx={{
          p: 2
        }}
      >
        <Stack
          component="form"
          spacing={2}
          onSubmit={submit}
        >
          <Typography variant="h4">Sign in</Typography>
          <TextField 
            name="email"
            label="Email"
            type="email"
            variant="filled"
            required
            fullWidth 
          />
          <TextField 
            name="password"
            label="Password"
            type="password"
            variant="filled"
            required
            fullWidth 
          />
          <Button
            type="submit"
            variant="contained"
            color="primary"
            endIcon={requestState === "pending" && <CircularProgress color="inherit" size="20px" />}
          >
            Sign in
          </Button>
        </Stack>
      </Paper>

      <Button
        variant="text"
        color="secondary"
        onClick={() => nav("/register")}
        sx={{
          mt: 1
        }}
      >
        Create an account...
      </Button>
    </Container>

    <Footer/>
    <CenterModal
      open={requestState === 'failure'}
      onClose={() => setRequestState(null)}
    >
      <Paper sx={{
        p: 2
      }}>
        <Typography variant="body">Login failed</Typography>
      </Paper>
    </CenterModal>
  </PageContainer>)
}

export default Login