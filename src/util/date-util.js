function parseDate(date) {
  const year = date.getFullYear().toString()
  const month = (date.getMonth() + 1).toString().padStart(2, "0")
  const day = date.getDate().toString().padStart(2, "0")

  return `${year}-${month}-${day}`
}

function deparseDate(dateString) {
  const exp = /^(\d{4})-(\d{2})-(\d{2})$/
  const match = exp.exec(dateString)
  if (match) {
    const year = parseInt(match[1])
    const month = parseInt(match[2]) - 1
    const day = parseInt(match[3])
    return new Date(year, month, day)
  } else {
    throw new Error("Invalid date string")
  }
}

const sameDay = (d1, d2) => (d1.getYear() === d2.getYear()
  && d1.getMonth() === d2.getMonth()
  && d1.getDate() === d2.getDate())

const daysOfWeek = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]
const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
  
export {
  parseDate, deparseDate, sameDay, daysOfWeek, months
}