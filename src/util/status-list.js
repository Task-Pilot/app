const statuses = {
  0: "Not started",
  1: "In progress",
  2: "Completed",
}

export default statuses