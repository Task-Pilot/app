import { deparseDate, sameDay, daysOfWeek, months } from "./date-util"


const groupTasks = (tasks) => {
  const groups = []

  const today = new Date()
  const tomorrow = new Date()
  tomorrow.setDate(today.getDate() + 1)
  const yesterday = new Date()
  yesterday.setDate(today.getDate() - 1)

  tasks.forEach(task => {
    const taskDate = deparseDate(task.date)
    var group = groups.find(g => sameDay(g.date, taskDate))
    if (!group) {
      const day = daysOfWeek[taskDate.getDay()]
      const month = months[taskDate.getMonth()]
      const year = taskDate.getFullYear()
      var dayText = `${day}, ${month}. ${taskDate.getDate()}`
      var groupText = dayText

      if (today.getFullYear() !== year)
        groupText += `, ${year}`

      if (sameDay(taskDate, today)) {
        groupText = `Today (${dayText})`
      } else if (sameDay(taskDate, tomorrow)) {
        groupText = `Tomorrow (${dayText})`
      } else if (sameDay(taskDate, yesterday)) {
        groupText = `Yesterday (${dayText})`
      }

      group = {
        date: taskDate,
        text: groupText,
        tasks: []
      }
      groups.push(group)
    }
    group.tasks.push(task)
  })
  return groups
}

export default groupTasks